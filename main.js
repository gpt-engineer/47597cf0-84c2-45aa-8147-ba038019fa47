const taskForm = document.getElementById("task-form");
const taskInput = document.getElementById("task-input");
const taskList = document.getElementById("task-list");

// Array of rainbow colors
const colors = ["red", "orange", "yellow", "green", "blue", "indigo", "violet"];
let colorIndex = 0;

taskForm.addEventListener("submit", function (e) {
  e.preventDefault();
  if (taskInput.value) {
    const newTask = document.createElement("li");
    newTask.textContent = taskInput.value;
    newTask.classList.add(
      "p-3",
      "rounded-lg",
      "flex",
      "justify-between",
      "items-center",
    );
    newTask.style.backgroundColor = colors[colorIndex];
    colorIndex = (colorIndex + 1) % colors.length; // Cycle through colors
    const deleteButton = document.createElement("button");
    deleteButton.innerHTML = '<i class="fas fa-trash-alt"></i>';
    deleteButton.classList.add("text-red-500");
    deleteButton.addEventListener("click", function () {
      taskList.removeChild(newTask);
    });
    newTask.appendChild(deleteButton);
    taskList.appendChild(newTask);
    taskInput.value = "";
  }
});
